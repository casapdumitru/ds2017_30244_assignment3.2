package dvd.service;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import dvd.entities.DVD;

public class FileService {
	public void CreateFile(DVD dvd) throws FileNotFoundException, UnsupportedEncodingException {
		DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
		Calendar cal = Calendar.getInstance();
		PrintWriter dvdFile = new PrintWriter(dvd.getTitle()+"_" + dateFormat.format(cal.getTime())+".txt", "UTF-8");
		dvdFile.println("Title: " + dvd.getTitle());
		dvdFile.println("Year: " + dvd.getYear());
		dvdFile.println("Price: " + dvd.getPrice());
		dvdFile.close();
	}
}
