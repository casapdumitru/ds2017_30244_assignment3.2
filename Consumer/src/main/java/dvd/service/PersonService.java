package dvd.service;

import java.util.ArrayList;
import java.util.List;

import dvd.entities.Person;

public class PersonService {

	public List<Person> GetAllPersons() {
		Person p1 = new Person("Dumitru", "Casap","casapdumitru@yahoo.com");
		Person p2 = new Person("Dima", "Casap","casapdumitru@gmail.com");
		Person p3 = new Person("Dumi", "Casap","casapdumitru@mail.ru");
		
		List<Person> persons = new ArrayList<Person>();
		persons.add(p1);
		persons.add(p2);
		persons.add(p3);
		
		return persons;
	}
}
