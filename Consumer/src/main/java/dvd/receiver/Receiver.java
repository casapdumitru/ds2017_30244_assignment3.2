package dvd.receiver;

import java.io.IOException;
import java.util.List;

import org.apache.commons.lang3.SerializationUtils;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

import dvd.entities.DVD;
import dvd.entities.Person;
import dvd.service.FileService;
import dvd.service.MailService;
import dvd.service.PersonService;

public class Receiver {

	private final static String QUEUE_NAME = "DVD";

	public static void main(String[] argv) throws Exception {
		final FileService fileService = new FileService();
		final PersonService personService = new PersonService();
		final MailService mailService = new MailService("casapdumitru1995@gmail.com", "dum199510");

		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("localhost");
		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();
		
		channel.exchangeDeclare("dvd_exchange","direct");
		channel.queueDeclare(QUEUE_NAME, false, false, false, null);
		System.out.println("Consumer! Waiting for dvd.");
		
		channel.queueBind(QUEUE_NAME, "dvd_exchange", "dvd-add");
		Consumer consumer = new DefaultConsumer(channel) {
			@Override
			public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
					byte[] body) throws IOException {
				DVD dvd = SerializationUtils.deserialize(body);

				String mailTitle = "New DVD was added ( " + dvd.getTitle() + " )";
				String mailContent = "Name: " + dvd.getTitle() + "\nYear: " + dvd.getYear() + "\nPrice "
						+ dvd.getPrice();

				List<Person> persons = personService.GetAllPersons();
				for (Person person : persons) {
					mailService.sendMail(person.getEmail(), mailTitle, mailContent);
				}

				fileService.CreateFile(dvd);

				System.out.println("Name: " + dvd.getTitle() + "\nYear: " + dvd.getYear() + "\nPrice " + dvd.getPrice());

			}
		};
		channel.basicConsume(QUEUE_NAME, true, consumer);
	}
}
