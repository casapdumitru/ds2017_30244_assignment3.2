package servlet;


import java.io.IOException;
import java.util.concurrent.TimeoutException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adding.Send;
import adding.StringConverter;


@WebServlet(urlPatterns = { "/dvd" })
public class DvdServlet extends HttpServlet {
	
	private Send send;
	
	public DvdServlet() {
		send = new Send();
	}
	
   public void doGet(HttpServletRequest request, HttpServletResponse response)
		      throws ServletException, IOException {
	   request.setAttribute("error", "");
	}
   public void doPost(HttpServletRequest request, HttpServletResponse response)
		      throws ServletException, IOException {
	   
	   /*request.getSession().setAttribute("error", "Error to login");

	   ConnectionFactory factory = new ConnectionFactory();
		  factory.setHost("localhost");
		  Connection connection = null;
		try {
			System.out.println("aaaaaa");
			connection = factory.newConnection();
		} catch (TimeoutException e) {
			e.printStackTrace();
		}
		  Channel channel = connection.createChannel();
		  
		  channel.queueDeclare(QUEUE_NAME, false, false, false, null);
		  String s = "aaaaaaaaaaaaaaa";
		  DVD d = new DVD("DVD!",32,1000);
		  byte [] data = SerializationUtils.serialize(d);
		  channel.basicPublish("", QUEUE_NAME, null, data);
		  DVD d1 = (DVD) SerializationUtils.deserialize(data);
		  try {
			channel.close();
		} catch (TimeoutException e) {
			e.printStackTrace();
		}
		  connection.close();*/
	   String title = request.getParameter("title");
	   String yearString = request.getParameter("year");
	   String priceString = request.getParameter("price");
	   
	   if(StringConverter.isInteger(yearString) && StringConverter.isDouble(priceString)) {
		   int year = Integer.parseInt(yearString);
		   double price = Double.parseDouble(priceString);
		   try {
				send.send(title,year,price);
				request.getSession().setAttribute("error", "Was added");
			} catch (TimeoutException e) {
				// TODO Auto-generated catch block
				request.getSession().setAttribute("error", "Error to add DVD. Try again!");
				e.printStackTrace();
			}
		   
	   } else {
		   request.getSession().setAttribute("error", "Error to add DVD. Year should be an integer and price a double value!");
	   }
	     response.sendRedirect(request.getContextPath());
	
   }
   

	
}
