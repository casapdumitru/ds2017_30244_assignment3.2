package adding;

import java.util.concurrent.TimeoutException;

import org.apache.commons.lang3.SerializationUtils;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import dvd.entities.DVD;


public class Send {
	private final static String QUEUE_NAME = "DVD";
	
	public void send(String title, int year, double price)
		      throws java.io.IOException, TimeoutException {
			  
			  ConnectionFactory factory = new ConnectionFactory();
			  factory.setHost("localhost");
			  Connection connection = factory.newConnection();
			  Channel channel = connection.createChannel();
			  
			  channel.exchangeDeclare("dvd_exchange","direct");
			  
			  
			  channel.queueDeclare(QUEUE_NAME, false, false, false, null);
			  channel.queueBind(QUEUE_NAME, "dvd_exchange", "dvd-add");
			  DVD d = new DVD(title, year, price);
			  byte[] data = SerializationUtils.serialize(d);
			  channel.basicPublish("", QUEUE_NAME, null, data );
			  channel.close();
			  connection.close();
		  }
}
